$(document).ready(function() {
    var CHAT_INPUT_MAX_COL = 41;

    var User = new function() {
        return {
            username: 'Miko'
        };
    };

    var Chat = new function() {
        var firebase = new Firebase('http://gamma.firebase.com/miko/social_rts/chat/message_list');

        var Chatbox = new function() {
            var cb = $('#chat #history'),
                MAX_HEIGHT = 200;

            function purgeOld() {
                
                var purgees = [],
                    purgeAmount = 0,
                    i = 0;

                while (cb.height() - purgeAmount > 200) {
                    var purgee = cb.children().eq(i);
                    purgeAmount += purgee.height();
                    purgees.push(purgee);
                }

                if (purgees.length > 0) {
                    _.each(purgees, function(purgee) {
                        purgee.fadeOut(null, function() {
                            $(this).remove();
                        });
                    });
                }
            }

            return {
                push: function(username, msg) {
                    var ele = $('<p style="display:none"><em>' + username + ': </em>' +
                        msg + '</p>');
                    cb.append(ele);
                    ele.fadeIn();
                    purgeOld();
                }
            }
        };

        function recvMsg(username, msg) {
            Chatbox.push(username, msg);
        }

        function init() {
            firebase.on('child_added', function(msgSnap) {
                var msgData = msgSnap.val()
                recvMsg(msgData.username, msgData.msg);
            });
        }

        init();

        return {
            sendMsg: function(msg) {
                var username = User.username;

                firebase.push({username: username, msg: msg});
            }
        };
    };

    var chatInputRows = 1;
    $('#chat textarea').keydown(function(evt) {
        var enter = false;
        if (evt.which == 13) {
            // Enter pressed
            Chat.sendMsg(this.value);
            this.value = '';
            enter = true;
        } 
        var newChatInputRows = Math.ceil(this.value.length / CHAT_INPUT_MAX_COL)
                        || 1;
        if (chatInputRows !== newChatInputRows) {
            chatInputRows = newChatInputRows;
            $('#chat #input_area').css('height', chatInputRows * 15);
            $('#chat #history').css('bottom', chatInputRows * 15);
        }

        return !enter;
    });

    var paper = Raphael(0, 0, $(window).width(), $(window).height());

    function Unit() {
        var that = {};
        that.x = 0;
        that.y = 0;
        return that;
    }

    Unit.prototype.move = function() {

    };

    var Me = new function() {
        var _g = paper.circle(50, 50, 10),
            VEL = 3,
            _p = paper.path("M50 50L60 60");
        _p.attr({
            'stroke-width': 3,
            fill: '#000'
        });
        _p.lx = 10;
        _p.ly = 10;

        _g.attr({
            fill: '#000',
            stroke: '#333'
        });

        _g.vx = 0;
        _g.vy = 0;
        _g.children = [_p];

        return {
            move: function(/*str*/ dir, /*bool*/ move) {
                if (move) {
                    _forward = true;
                    _g.vx = VEL;
                } else {
                    _g.vx = 0;
                }
            },
            moveable: _g
        }
    };

    var moveables = [Me.moveable];

    function tick() {
        _.each(moveables, function(moveable) {
            var prevX = moveable.attr('cx'),
                prevY = moveable.attr('cy'),
                newX = prevX + moveable.vx,
                newY = prevY + moveable.vy;

            update.call(moveable);
            _.each(moveable.children, function(child) {
                update.call(child);
            });

            function update() {
                if ($(this.node).is('circle')) {
                    this.attr({
                        cx: newX,
                        cy: newY
                    });
                } else if ($(this.node).is('path')) {
                    this.attr({
                        path: 'M' + newX + ' ' + newY + 'L' + 
                                (newX + this.lx) + ' ' +
                                (newY + this.ly)
                    });
                }
            }

        });
    }

    var gameClock = setInterval(tick, 30);

    var KEYS = {
        W: 87,
        A: 65,
        S: 83,
        D: 68,
        SHIFT: 16,
        CTRL: 17,
        ONE: 49,
        SPACE: 32
    };

    $(document).keydown(function(evt) {
        if (evt.which == KEYS.W) {
            Me.move('forward', true);
        } else if (evt.which == KEYS.S) {
            Me.move('backward', true);
        } else if (evt.which == KEYS.A) {
            Me.move('left', true);
        } else if (evt.which == KEYS.D) {
            Me.move('right', true);
        }
    });

    $(document).keyup(function(evt) {
        if (evt.which == KEYS.W) {
            Me.move('forward', false);
        } else if (evt.which == KEYS.S) {
            Me.move('backward', false);
        } else if (evt.which == KEYS.A) {
            Me.move('left', false);
        } else if (evt.which == KEYS.D) {
            Me.move('right', false);
        }
    });
});
