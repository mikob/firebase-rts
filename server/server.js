var app = require('http').createServer(handler),
    fs = require('fs'),
    path = require('path');

app.listen(4000);

function handler(request, response) {
    var filePath = '.' + request.url;
    console.log('filepath: ' + filePath);

    if (filePath == './') {
        filePath = './social-rts.html';
        console.log('Redirecting to ' + filePath);
    }

    filePath = '.' + filePath;

    path.exists(filePath, function(exists) {
        console.log('Path exists? ' + exists);
        if (exists)
        {
            fs.readFile(path.join(__dirname, filePath),
                function (err, data) {
                    if (err) {
                        response.writeHead(500);
                        response.end('Error loading ' + filePath);
                    } else {
                        response.writeHead(200);
                        response.end(data);
                    }
                });
        } else {
            response.writeHead(404);
            response.end();
        }
    });
}
